<?php

$id = $_REQUEST["id"];
$result = "";
$subscriptionArray = array();
$resultArray = [];
if ($id !== "") {
    require 'lib/Stripe.php';
    Stripe::setApiKey("sk_test_JpXpshSVW3EcBHoTgRgnFOHn");
    try {
        $json = Stripe_Customer::retrieve($id);
        $customerData = json_decode($json, true);
        $customerName = $customerData['email'];
        $subscriptions = $customerData['subscriptions'];
        $description =  $customerData['description'];
        if (isset($subscriptions['total_count']) && $subscriptions['total_count'] > 0) {
            $data = $subscriptions['data'];
            foreach ($data as $obj) {
                $subscriptionId = $obj['id'];
                $endDate = $obj['current_period_end'];
                if ($result === "") {
                    $result = $customerName . "<br/>subscription: " . $subscriptionId . "<br/>Expires on : " . date('m/d/Y', $endDate);
                } else {
                    $result = $result . "<br/>" . $customerName . "<br/>subscription: " . $subscriptionId . "<br/>Expires on: " . date('m/d/Y', $endDate);
                }

                $plan = $obj['plan'];

                array_push($subscriptionArray, array("subscriptionId" => $subscriptionId, "subscriptionName" => $plan['name'], "expiryDate" => date('m/d/Y', $endDate)));
            }
            
            $resultArray = json_encode(array("customerId" => $id,"description"=> $description, "customerEmail" => $customerName, "subscriptions" => $subscriptionArray, "createdOn" => date('m/d/Y', $customerData['created'])));
            $result = $resultArray;
        } else {
            $result = "false";
        }
    } catch (Stripe_CardError $e) {
        // Since it's a decline, Stripe_CardError will be caught
        $body = $e->getJsonBody();
        $err = $body['error'];

        print('Status is:' . $e->getHttpStatus() . "\n");
        print('Type is:' . $err['type'] . "\n");
        print('Code is:' . $err['code'] . "\n");
        // param is '' in this case
        print('Param is:' . $err['param'] . "\n");
        print('Message is:' . $err['message'] . "\n");
    } catch (Stripe_InvalidRequestError $e) {
        $result = "invalid";
        // Invalid parameters were supplied to Stripe's API
        //write_exception($e);
    } catch (Stripe_AuthenticationError $e) {
        // Authentication with Stripe's API failed
        // (maybe you changed API keys recently)
        $body = $e->getJsonBody();
        $err = $body['error'];

        print('Status is:' . $e->getHttpStatus() . "\n");
        print('Type is:' . $err['type'] . "\n");
        print('Code is:' . $err['code'] . "\n");
        // param is '' in this case
        print('Param is:' . $err['param'] . "\n");
        print('Message is:' . $err['message'] . "\n");
    } catch (Stripe_ApiConnectionError $e) {
        // Network communication with Stripe failed
        $body = $e->getJsonBody();
        $err = $body['error'];

        print('Status is:' . $e->getHttpStatus() . "\n");
        print('Type is:' . $err['type'] . "\n");
        print('Code is:' . $err['code'] . "\n");
        // param is '' in this case
        print('Param is:' . $err['param'] . "\n");
        print('Message is:' . $err['message'] . "\n");
    } catch (Stripe_Error $e) {
        // Display a very generic error to the user, and maybe send
        // yourself an email
        $body = $e->getJsonBody();
        $err = $body['error'];

        print('Status is:' . $e->getHttpStatus() . "\n");
        print('Type is:' . $err['type'] . "\n");
        print('Code is:' . $err['code'] . "\n");
        // param is '' in this case
        print('Param is:' . $err['param'] . "\n");
        print('Message is:' . $err['message'] . "\n");
    } catch (Exception $e) {
        // Something else happened, completely unrelated to Stripe
        // Since it's a decline, Stripe_CardError will be caught
        $body = $e->getJsonBody();
        $err = $body['error'];

        print('Status is:' . $e->getHttpStatus() . "\n");
        print('Type is:' . $err['type'] . "\n");
        print('Code is:' . $err['code'] . "\n");
        // param is '' in this case
        print('Param is:' . $err['param'] . "\n");
        print('Message is:' . $err['message'] . "\n");
    }
}

echo $result === "" ? "Please enter a valid customer id" : $result;
?>