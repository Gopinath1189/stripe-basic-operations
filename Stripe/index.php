<?php

include 'header.php';
?>

<style>
    table tbody tr td{
        text-align:left;
    }
</style>

<div class="container text-center">    
    <h3>Customer Forms</h3><br>
    <div class="row">
        <div class="col-md-8 col-md-push-3">
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-4">
                        <input type="text" id="txtCustomerId" class="form-control" placeholder="Enter customer id" />
<!--                        <input type="text" id="txtCustomerEmail" class="form-control" placeholder="Enter customer email" />-->
                    </div>
                    <div class="col-md-8 text-left">
                        <button type="button" id="btnSubmit" class="btn btn-primary">Get Customer</button>
                        <button type="button" id="btnSubscriptionSubmit" class="btn btn-warning">Get Subscription</button>
                        <button type="button" id="btnSubscriptionDelete" class="btn btn-danger">Delete Subscription</button>
                    </div>
                </div>

                <div id="resultTable" style="display: none;">
                    <h4 class="text-left">Output: </h4>
                    <table class="table table-bordered table-hover" id="customerTable" style="display: none;">
                        <thead>
                            <tr class="info">
                                <th>Customer Id</th>
                                <th>Description</th>
                                <th>Email</th>
                                <th>Created On</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-hover" id="subscriptionTable" style="display: none;">
                        <thead>
                            <tr class="info">
                                <th>Subscription Id</th>
                                <th>Plan Name</th>
                                <th>Email</th>
                                <th>End Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div id="result" class="col-md-12" style="color:#fff !important; text-align: left !important;">

                </div>
                <div id="domMessage" style="display:none;"> 
                    <h4>We are processing your request.  Please be patient.</h4> 
                </div>
            </form>
        </div>
    </div>
</div><br>

<script>

    function showCustomerStatus() {

        //Getting the customer id from the 
        str = $("#txtCustomerId").val();

        if (str.length === 0) {
            alert("Please provide customer Id.");
            return;
        } else {
            //Creating a request.
            var xmlhttp = new XMLHttpRequest();
            //Getting response
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    $.blockUI({message: $('#domMessage')});
                    $result = xmlhttp.responseText;
                    if ($result === "false")
                    {
                        $("#result").html("The customer has no active subscription.");
                        $("#result").addClass("label-danger");
                        $("#result").removeClass("label-success");
                    } else if ($result === "invalid") {
                        $("#result").html("The customer Id is invalid.");
                        $("#result").addClass("label-danger");
                        $("#result").removeClass("label-success");
                    } else
                    {
                        $('#customerTable tbody').html('');
                        $('#subscriptionTable tbody').html('');
                        var temp = JSON.parse($result);
                        $('#customerTable tbody').append('<tr><td>' + temp.customerId + '</td><td>' + temp.description + '</td><td>' + temp.customerEmail + '</td><td>' + temp.createdOn + '</td></tr>');
                        $subs = temp.subscriptions;
                        $.each($subs, function (idx, obj) {
                            $('#subscriptionTable tbody').append('<tr><td>' + obj.subscriptionId + '</td><td>' + obj.subscriptionName + '</td><td>' + temp.customerEmail + '</td><td>' + obj.expiryDate + '</td></tr>');
                        });
                        $('#resultTable').show();
                        $('#customerTable').show();
                        $('#subscriptionTable').show();
//                        $("#result").html($result);
//                        $("#result").addClass("label-success");
//                        $("#result").removeClass("label-danger");
                    }
                    $.unblockUI();
                }

            };
            //Opening connection
            xmlhttp.open("GET", "stripe_check_customer_subscription.php?id=" + str, true);
            //Sending the request
            xmlhttp.send();
        }
    }

    function showSubscriptionStatus() {

        //Getting the customer id from the 
        str = $("#txtCustomerId").val();

        if (str.length === 0) {
            alert("Please provide customer Id.");
            return;
        } else {
            //Creating a request.
            var xmlhttp = new XMLHttpRequest();
            //Getting response
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    $result = xmlhttp.responseText;
                    if ($result === "false")
                    {
                        $("#result").html("The customer has no active subscription.");
                        $("#result").addClass("label-danger");
                        $("#result").removeClass("label-success");
                    } else if ($result === "invalid") {
                        $("#result").html("The customer Id is invalid.");
                        $("#result").addClass("label-danger");
                        $("#result").removeClass("label-success");
                    } else
                    {
                        $('#subscriptionTable tbody').html('');
                        var temp = JSON.parse($result);
                        $subs = temp.subscriptions;
                        $.each($subs, function (idx, obj) {
                            $('#subscriptionTable tbody').append('<tr><td>' + obj.subscriptionId + '</td><td>' + obj.subscriptionName + '</td><td>' + temp.customerEmail + '</td><td>' + obj.expiryDate + '</td></tr>');
                        });
                        $('#resultTable').show();
                        $('#customerTable').hide();
                        $('#subscriptionTable').show();
                    }
                    $.unblockUI();
                }

            };
            //Opening connection
            xmlhttp.open("GET", "stripe_check_customer_subscription.php?id=" + str, true);
            //Sending the request
            xmlhttp.send();
        }
    }
    
    function showDeleteSubscription() {

        //Getting the customer id from the 
        str = $("#txtCustomerId").val();

        if (str.length === 0) {
            alert("Please provide customer Id.");
            return;
        } else {
            //Creating a request.
            var xmlhttp = new XMLHttpRequest();
            //Getting response
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    $result = xmlhttp.responseText;
                    if ($result === "false")
                    {
                        $("#result").html("The customer has no active subscription.");
                        $("#result").addClass("label-danger");
                        $("#result").removeClass("label-success");
                    } else if ($result === "invalid") {
                        $("#result").html("The customer Id is invalid.");
                        $("#result").addClass("label-danger");
                        $("#result").removeClass("label-success");
                    } else
                    {
                        $("#result").html($result);
                        $("#result").addClass("label-success");
                        $("#result").removeClass("label-danger");
                        $('#resultTable').hide();
                        $('#customerTable').hide();
                        $('#subscriptionTable').hide();
                    }
                    $.unblockUI();
                }

            };
            //Opening connection
            xmlhttp.open("GET", "stripe_delete_customer_subscription.php?id=" + str, true);
            //Sending the request
            xmlhttp.send();
        }
    }

    $("#btnSubmit").on("click", function () {
        $.blockUI({message: $('#domMessage')});
        showCustomerStatus();
    });
    $("#btnSubscriptionSubmit").on("click", function () {
        $.blockUI({message: $('#domMessage')});
        showSubscriptionStatus();
    });
    $("#btnSubscriptionDelete").on("click", function () {
        $.blockUI({message: $('#domMessage')});
        showDeleteSubscription();
    });
</script>
<?php

include 'footer.php';
?>